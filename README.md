## Prereq.
* EventHub Created in Azure with a connections string
* Storage account created with a blob container inside it along with connection string
## Instructions
* Clone the repo
    * git clone https://gitlab.com/ubs-workshop-batch-5/azure-eventhub-python
    * cd azure-eventhub-python
* Install python libraries
    * pip3 install azure-eventhub azure-eventhub-checkpointstoreblob-aio
* Update the conntection string of EventHub in send.py
    * vi send.py
* Update the connection strings of Event Hub and Storage Account in recv.py
    * vi recv.py
* Execute the code in follwing sequence, first to send the data, then to look upon receiving it
    * python3 send.py
    * python3 rec.py